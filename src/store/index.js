import { createStore } from 'vuex'
import axios from "axios"

export default createStore({
  state: {
    posts: []
  },
  mutations: {
    setPosts(state, posts) {
      state.posts = posts
    }
  },
  actions: {
    async fetchPosts({commit}) {
      try {
        setTimeout(async () => {
          let {data} = await axios.get('http://jsonplaceholder.typicode.com/posts')
          commit('setPosts', data)
        }, 2000)
      } catch (error) {
        console.log(error)
      }
      // commit('setPosts', data)
      // axios.get('http://jsonplaceholder.typicode.com/posts')
      // .then((data) => {
      //   setTimeout(()=>{
      //     commit('setPosts', data.data)
      //   }, 2000)
      // })
      // .catch((error) => {
      //   console.log(error)
      // })
    }
  },
  getters: {
    getPosts(state) {
      return state.posts
    }
  }
})
